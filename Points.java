package punkt;

public class Points {
    private String name;
    private double x;
    private double y;
    private double distance;

    public Points(String name, double x, double y) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.distance = Math.sqrt(Math.pow(Math.abs(x), 2) + Math.pow(Math.abs(y), 2));
    }

    protected String getName() {
        return name;
    }

    protected double getX() {
        return x;
    }

    protected double getY() {
        return y;
    }

    protected double getDistance() {
        return distance;

    }
}

