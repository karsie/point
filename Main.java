package punkt;

import punkt.Points;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        calculate();
    }

    private static void calculate() {
        List<Points> points = sort(colektPoint(setPointNumber()));
        points.forEach(point -> System.out.println(point.getName() + ": x = " + point.getX() + ", y = " + point.getY() + ", distance = " + point.getDistance()));
    }

    private static List<Points> sort(List<Points> point) {
        if (point == null || point.size() == 0) {
            return null;
        }
        final Comparator<Points> pointsComparator = (o1, o2) -> (int) (Math.round(o1.getDistance()) - Math.round(o2.getDistance()));
        point.sort(pointsComparator);
        return point;
    }

    private static List<Points> colektPoint(int a) {
        List<Points> points = new ArrayList<>();
        for (int i = 0; i < a; i++) {
            Points points1 = new Points(setName(), setCoordinate(), setCoordinate2());
            points.add(points1);
        }
        return points;
    }

    private static int setPointNumber() {
        System.out.print("podaj liczbę punktów: ");
        return scanner.nextInt();
    }

    private static String setName() {
        System.out.print("podaj nazwę punktu: ");
        return scanner.next();
    }

    private static double setCoordinate() {
        System.out.println("podaj pierwszą współrzędna: ");
        return scanner.nextDouble();
    }

    private static double setCoordinate2() {
        System.out.println("podaj druga współrzędna: ");
        return scanner.nextDouble();
    }
}


